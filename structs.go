package main

import (
	"net/http"
	"time"
)

type handle404 struct {
}

func (_ handle404) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	data := map[string]interface{}{
		"URL": r.URL.RequestURI(),
	}
	templates.ExecuteTemplate(w, "404.html", data)
}

type user struct {
	// Public
	ID       int       `json:"id"`
	Username string    `json:"username"`
	LastSeen time.Time `json:"last_seen"`

	// Private
	Password string
}

type thread struct {
	ID        int       `json:"id"`
	Communty  int       `json:"parent_community"`
	Author    string    `json:"author"`
	Title     string    `json:"title"`
	Body      string    `json:"body"`
	CreatedAt time.Time `json:"created"`
}

type comment struct {
	ID        int       `json:"id"`
	Thread    int       `json:"parent_thread"`
	Author    string    `json:"author"`
	Body      string    `json:"body"`
	CreatedAt time.Time `json:"created"`
}

type community struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

package main

import (
	"net/http"
	"time"
)

var testThreads = []thread{
	{ID: 0, Author: "0", Title: "a1", Body: "b1", CreatedAt: time.Now()},
	{ID: 1, Author: "0", Title: "a2", Body: "b2", CreatedAt: time.Now()},
	{ID: 2, Author: "0", Title: "a3", Body: "b3", CreatedAt: time.Now()},
}

func listThreads(w http.ResponseWriter, r *http.Request) {
	data := map[string]interface{}{
		"Threads": testThreads,
	}
	templates.ExecuteTemplate(w, "threads.html", data)
}

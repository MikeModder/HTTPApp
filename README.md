# HTTPApp
### What is this
This is my playground for making a web app using Golang.
### Project goal
My rough end goal is to rebuild [SSMT Reloaded](https://gitlab.com/MikeModder/SSMT-Reloaded) in Golang.
### Notes and other stuff
* Complete rewrite of code (NodeJS to Golang)
* Use SQL for storage (likely PostgreSQL)
* Port all existing features from SSMT Reloaded.
### Wishlist
* File uploading (redirect files to external pomf(?) server)
* Pack assets/views using `go-bindata`?
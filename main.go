package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

var (
	port      = 8080
	templates *template.Template
	db        *sql.DB
)

func main() {
	// Connect to the database
	var err error
	db, err = sql.Open("postgres", "postgres://mike@localhost/mike?sslmode=disable")
	if err != nil {
		log.Fatalf("[error] failed to connect to database: %v\n", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalf("[error] failed to ping database: %v\n", err)
	}

	// Get a new router
	r := mux.NewRouter()

	// Set up our routes
	// Index
	r.HandleFunc("/", index)
	// User-related
	r.HandleFunc("/login", login)
	r.HandleFunc("/register", notImpl)
	r.HandleFunc("/user/{username}", notImpl)
	// Thread-related
	r.HandleFunc("/threads", listThreads)
	r.HandleFunc("/threads/{id:[0-9]+}", notImpl)
	r.HandleFunc("/threads/{id:[0-9]+}/new", notImpl)
	r.HandleFunc("/threads/new", notImpl)

	// Set a 404 handler
	r.NotFoundHandler = handle404{}

	// Handle static files
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))

	// Use the mux router
	http.Handle("/", r)

	// Load our templates
	templates = template.Must(template.ParseGlob("views/*.html"))

	// And we're off to the races!
	log.Printf("[info] listening on port %d\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}

func notImpl(w http.ResponseWriter, r *http.Request) {
	data := map[string]interface{}{
		"URL": r.URL.RequestURI(),
	}
	templates.ExecuteTemplate(w, "notyet.html", data)
}

package main

import (
	"net/http"

	sessions "github.com/kataras/go-sessions"
	"golang.org/x/crypto/bcrypt"
)

func index(w http.ResponseWriter, r *http.Request) {
	s := sessions.Start(w, r)
	if s.GetString("userid") == "" {
		templates.ExecuteTemplate(w, "index.html", nil)
		return
	}

	data := map[string]interface{}{
		"Error": s.GetFlash("error"),
		"Info":  s.GetFlash("info"),
		"User":  getUser(s.GetString("username")),
	}
	templates.ExecuteTemplate(w, "index.html", data)
}

func login(w http.ResponseWriter, r *http.Request) {
	s := sessions.Start(w, r)
	if s.GetString("userid") != "" {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	if r.Method != http.MethodPost {
		// It's not a post request so we should show them the login page
		templates.ExecuteTemplate(w, "login.html", nil)
		return
	}

	r.ParseForm()

	username := r.FormValue("username")
	password := r.FormValue("password")

	user := getUser(username)

	if e := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); e == nil {
		// The passwords matched
		s.Set("userid", user.ID)
		s.Set("username", user.Username)
		s.SetFlash("info", "Logged in!")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	} else {
		templates.ExecuteTemplate(w, "login.html", map[string]interface{}{
			"Error": "Incorrect login!",
		})
	}
}

func register(w http.ResponseWriter, r *http.Request) {

}

package main

// password = $2b$10$gFwIwwRJTBfAqFIqBs3yx.Ok.YrkY8Qy8jEYsj/punw7r0wHMo4Hq

var users = []user{
	{Username: "mike", Password: "$2a$12$8xN8WaLOUhvUQIO22lzJCOFWOp1Eu7uYweo.z2CxawwFRQmIu3M4G"},
}

func getUser(username string) user {
	u := user{}
	db.QueryRow("SELECT id, username, last_seen, password FROM users WHERE username = ?", username).
		Scan(&u.ID, &u.Username, &u.LastSeen, &u.Password)

	return u
}

func getThread(id int) thread {
	t := thread{}
	db.QueryRow("SELECT id, author, title, body, created_at, community FROM threads INNER JOIN users ON users.id = author WHERE id = ?", id).
		Scan(&t.ID, &t.Author, &t.Title, &t.Body, &t.CreatedAt)

	return t
}

func getCommunity(id int) community {
	c := community{}
	db.QueryRow("SELECT id, name, desc FROM communities WHERE id = ?", id).
		Scan(&c.ID, &c.Name, &c.Description)

	return c
}

func getCommunityThreads(communityid int) []*thread {
	threadRows, _ := db.Query("SELECT id, author, title, body, created_at FROM threads INNER JOIN users ON user.id = author WHERE community = ?", communityid)
	var threads []*thread

	for threadRows.Next() {
		t := &thread{}

		err := threadRows.Scan(&t.ID, &t.Author, &t.Title, &t.Body, &t.CreatedAt)
		if err != nil {
			return []thread{}
		}
		t.Communty = communityid

		threads = append(threads, t)
	}

	threadRows.Close()

	return threads
}

func getCommentCount(threadid int) int {
	var count int
	db.QueryRow("SELECT COUNT(*) FROM comments WHERE thread = ?", threadid).
		Scan(&count)

	return count
}
